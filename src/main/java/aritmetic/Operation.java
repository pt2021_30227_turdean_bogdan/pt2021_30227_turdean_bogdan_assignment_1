package aritmetic;

import javax.swing.*;
import java.util.ArrayList;

public class Operation {

    public static Polinom addPoli(Polinom p1, Polinom p2){
        Polinom result = new Polinom(p1.getTermeni());
        result.getTermeni().addAll(p2.getTermeni());
        result.restrange();
        result.ordoneaza();
        return result;
    }

    public static Polinom substrPoli(Polinom p1, Polinom p2){
        ArrayList<Monom> aux = new ArrayList<>();
        for(Monom el: p1.getTermeni()){
            Monom altEl = new Monom(el.getPutere(), el.getCoeficient());
            aux.add(altEl);
        }
        for (Monom el : p2.getTermeni()) {
            Monom auxel = new Monom(el.getPutere(), -el.getCoeficient());
            aux.add(auxel);
        }
        Polinom result = new Polinom(aux);
        result.restrange();
        result.ordoneaza();
        return result;
    }

    public static Polinom multiplyPoli(Polinom p1, Polinom p2){
        ArrayList<Monom> aux = new ArrayList<>();
        for(Monom el: p1.getTermeni()){
            for(Monom el2: p2.getTermeni()){
                Monom auxEl = new Monom(el.getPutere()+el2.getPutere(),el.getCoeficient()*el2.getCoeficient());
                aux.add(auxEl);
            }
        }
        Polinom result = new Polinom(aux);
        result.restrange();
        result.ordoneaza();
        return result;
    }

    public static Polinom derivata(Polinom p){
        for(Monom el: p.getTermeni()){
            el.setCoeficient(el.getCoeficient() * el.getPutere());
            el.setPutere(el.getPutere()-1);
        }
        return p;
    }

    public static Polinom integrala(Polinom p){
        for(Monom el: p.getTermeni()){
            el.setPutere(el.getPutere()+1);
            el.setCoeficient(el.getCoeficient() / el.getPutere());
        }
        return p;
    }

    public static void divide(Polinom p1, Polinom p2, Polinom q, Polinom r){
        if(p2.grad()!=0 || p2.getTermeni().get(0).getCoeficient()!=0) {
            ArrayList<Monom> termeniQ = new ArrayList<>();
            q.setTermeni(termeniQ);
            Polinom aux2 = new Polinom(new ArrayList<>());
            while (p1.grad() >= p2.grad() && p1.toString().compareTo("") != 0 && p2.toString().compareTo("") != 0) {
                int putere = p1.getTermeni().get(0).getPutere() - p2.getTermeni().get(0).getPutere();
                float coeficient = p1.getTermeni().get(0).getCoeficient() / p2.getTermeni().get(0).getCoeficient();
                Monom elq = new Monom(putere, coeficient);
                termeniQ.add(elq);
                aux2.getTermeni().clear();
                aux2.getTermeni().add(elq);
                q.setTermeni(termeniQ);
                Polinom aux1 = Operation.multiplyPoli(p2, aux2);
                p1.setTermeni(Operation.substrPoli(p1, aux1).getTermeni());
            }
            r.setTermeni(p1.getTermeni());
        }
        else
            JOptionPane.showMessageDialog(null, "Divide by 0", "ERROR", JOptionPane.ERROR_MESSAGE);
    }

}
