package aritmetic;

import javax.swing.*;

public class Monom implements Comparable{
    private int putere;
    private float coeficient;

    public Monom(String monom) { // format: a*X^b || a (a*X-> a*X^1; X^b-> 1*X^b)
        if(monom.indexOf('*')!=-1)
            try {
                this.coeficient = Float.parseFloat(monom.substring(0, monom.indexOf('*')));
            }
            catch (NumberFormatException e){
                JOptionPane.showMessageDialog(null, "Eroare la input (number * X ^ number +- etc)", "ERROR", JOptionPane.ERROR_MESSAGE);
                this.coeficient = 0;
            }
        else
            try {
                this.coeficient = Float.parseFloat(monom);
            }
            catch (NumberFormatException e){
                JOptionPane.showMessageDialog(null, "Eroare la input (number * X ^ number +- etc)", "ERROR", JOptionPane.ERROR_MESSAGE);
                this.coeficient = 0;
            }
        if(monom.indexOf('^')!=-1)
            try{
                this.putere = Integer.parseInt(monom.substring(monom.indexOf('^')+1));
            }
            catch (NumberFormatException e){
                JOptionPane.showMessageDialog(null, "Eroare la input (number * X ^ number +- etc)", "ERROR", JOptionPane.ERROR_MESSAGE);
                this.putere = 0;
            }
        else
            try{
                this.coeficient = Float.parseFloat(monom);
                this.putere = 0;
            }
            catch (NumberFormatException e) {
                this.putere = 1;
            }
    }

    public Monom(int putere, float coeficient) {
        this.putere = putere;
        this.coeficient = coeficient;
    }

    public int getPutere() {
        return putere;
    }

    public float getCoeficient() {
        return coeficient;
    }

    public void setPutere(int putere) {
        this.putere = putere;
    }

    public void setCoeficient(float coeficient) {
        this.coeficient = coeficient;
    }

    @Override
    public String toString() {

        String rez = null;
        if(coeficient<0) {
            rez = " " + this.coeficient;
            if(this.putere != 0)
                rez = rez.concat("*X^" + this.putere);
        }
        else
            if(coeficient>=0) {
                rez = " +" + this.coeficient;
                if(this.putere != 0)
                    rez = rez.concat("*X^" + this.putere);
            }
            
        return rez;
    }

    @Override
    public int compareTo(Object x) {
        return ((Monom)x).getPutere()-this.putere;
    }
}
