package aritmetic;

import java.util.*;

public class Polinom {
    private ArrayList<Monom> termeni;

    public Polinom(ArrayList<Monom> termeni) {
        this.termeni = termeni;
        restrange();
        ordoneaza();
    }

    public Polinom(){
        this.termeni = new ArrayList<>();
    }

    public ArrayList<Monom> getTermeni() {
        return termeni;
    }

    public void setTermeni(ArrayList<Monom> termeni) {
        this.termeni = termeni;
        for(int i=0;i<this.termeni.size();i++)
            if(this.termeni.get(i).getCoeficient()==0)
                this.termeni.remove(this.termeni.get(i));
        restrange();
        ordoneaza();
    }

    public void restrange(){
        for (int i=0;i<this.termeni.size();i++) {
            for (int j=i+1;j<this.termeni.size();j++) {
                if(this.termeni.get(i).getPutere()==this.termeni.get(j).getPutere()){
                    this.termeni.get(i).setCoeficient(this.termeni.get(i).getCoeficient()+this.termeni.get(j).getCoeficient());
                    this.termeni.remove(this.termeni.get(j));
                }
            }
        }
        /*for(Monom m1: this.termeni){
            for(Monom m2: this.termeni){
                if(m1.getPutere()==m2.getPutere() && m1.equals(m2)==false){
                    m1.setCoeficient(m1.getCoeficient()+m2.getCoeficient());
                    this.termeni.remove(m2);
                }
            }

        }*/
    }

    public int grad(){
        int rez = 0;
        for(Monom m: this.termeni){
            if(m.getPutere()>rez && m.getCoeficient()!=0)
                rez = m.getPutere();
        }
        return rez;
    }

    public void ordoneaza(){

        Collections.sort(this.termeni);
    }

    @Override
    public String toString() {
        String result="";
        for (Monom el : this.termeni) {
            if (el.getCoeficient()!=0)
                result = result.concat(el.toString());
        }
        return result;
    }
}
