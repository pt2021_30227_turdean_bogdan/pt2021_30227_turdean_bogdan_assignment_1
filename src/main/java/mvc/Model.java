package mvc;

import aritmetic.Monom;
import aritmetic.Operation;
import aritmetic.Polinom;

import java.util.ArrayList;

public class Model {
    private Polinom total;
    private Polinom[] totalD;

    public Model() {
        reset();
    }

    public void setTotalD(Polinom[] totalD){
        this.totalD = totalD;
    }

    public void setTotal(Polinom total) {
        this.total = total;
    }

    public void reset(){
        ArrayList<Monom> l = new ArrayList<>();
        Monom m = new Monom("0");
        l.add(m);
        total = new Polinom(l);
        totalD = new Polinom[2];
        totalD[0] = new Polinom(l);
        totalD[1] = new Polinom(l);
    }

    public Polinom convertToPoli(String s){
        Monom el;
        ArrayList<Monom> termeni = new ArrayList<>();
        String[] parts1 = s.split("-");
        for(String p: parts1){
            if(p.compareTo(parts1[0])!=0)
                p = "-".concat(p);
            String[] parts2 = p.split("\\+");
            for(String p2: parts2){
                if(p2.compareTo("")!=0){
                    el = new Monom(p2);
                    termeni.add(el);
                }
            }
        }
        return new Polinom(termeni);
    }

    public Polinom add(String s1, String s2){
        Polinom p1 = convertToPoli(s1);
        Polinom p2 = convertToPoli(s2);

        total = Operation.addPoli(p1, p2);

        return total;
    }

    public Polinom substr(String s1, String s2){
        Polinom p1 = convertToPoli(s1);
        Polinom p2 = convertToPoli(s2);

        total = Operation.substrPoli(p1, p2);

        return total;
    }

    public Polinom multiply(String s1, String s2){
        Polinom p1 = convertToPoli(s1);
        Polinom p2 = convertToPoli(s2);

        total = Operation.multiplyPoli(p1, p2);

        return total;
    }

    public Polinom[] divide(String s1, String s2 , Polinom q, Polinom r){
        Polinom p1 = convertToPoli(s1);
        Polinom p2 = convertToPoli(s2);

        Operation.divide(p1, p2, q, r);

        totalD[0] = q;
        totalD[1] = r;

        return totalD;
    }

    public Polinom derivare(String s){
        Polinom p = convertToPoli(s);

        total = Operation.derivata(p);

        return total;
    }

    public Polinom integrare(String s){
        Polinom p = convertToPoli(s);

        total = Operation.integrala(p);

        return total;
    }

}
