package mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import aritmetic.*;

public class Controller {
    private final Model model;
    private final View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;

        view.addPoliListener(new AddListener());
        view.substrPoliListener(new SubstrListener());
        view.multiplyPoliListener(new MultiplyListener());
        view.derivataPoliListener(new DerivareListener());
        view.integralaPoliListener(new IntegrareListener());
        view.dividePoliListener(new DivideListener());
        view.clearListener(new ClearListener());
    }

    class DivideListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String poli1;
            String poli2;

            poli1 = view.getUserInput(true);
            poli2 = view.getUserInput(false);
            Polinom q, r;
            q = new Polinom();
            r = new Polinom();
            Polinom[] result = new Polinom[2];
            result[0] = q;
            result[1] = r;
            result = model.divide(poli1, poli2, q, r);
            view.setTotalD(result);
            //view.reset();
            model.reset();
        }

    }

    class AddListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String poli1;
            String poli2;

            poli1 = view.getUserInput(true);
            poli2 = view.getUserInput(false);
            Polinom result = model.add(poli1, poli2);
            view.setTotal(result);
            //view.reset();
            model.reset();
        }

    }

    class IntegrareListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String poli;

            poli = view.getUserInput(true);
            Polinom result = model.integrare(poli);
            view.setTotal(result);
            model.reset();
        }
    }

    class SubstrListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String poli1;
            String poli2;

            poli1 = view.getUserInput(true);
            poli2 = view.getUserInput(false);
            Polinom result = model.substr(poli1, poli2);
            view.setTotal(result);
            //view.reset();
            model.reset();
        }
    }

    class MultiplyListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String poli1;
            String poli2;

            poli1 = view.getUserInput(true);
            poli2 = view.getUserInput(false);
            Polinom result = model.multiply(poli1, poli2);
            view.setTotal(result);
            //view.reset();
            model.reset();
        }
    }

    class DerivareListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String poli1;

            poli1 = view.getUserInput(true);
            Polinom result = model.derivare(poli1);
            view.setTotal(result);
            //view.reset();
            model.reset();
        }
    }

    class ClearListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            view.reset();
        }

    }
}
