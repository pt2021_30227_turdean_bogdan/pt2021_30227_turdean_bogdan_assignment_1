package mvc;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import aritmetic.Polinom;

public class View extends JFrame {
    private final JTextField inPoli1 = new JTextField(100);
    private final JTextField inPoli2 = new JTextField(100);
    private final JButton adunareBtn = new JButton("+");
    private final JButton scadereBtn = new JButton("-");
    private final JButton inmultireBtn = new JButton("*");
    private final JButton impartireBtn = new JButton("/");
    private final JButton derivareBtn = new JButton("Derivare");
    private final JButton integrareBtn = new JButton("Integrare");
    private final JTextField outPoli = new JTextField(100);
    private final JButton clearBtn = new JButton("Clear");
    private final Model model;


    public View(Model model) {
        this.model = model;

        JPanel panelTitlu = new JPanel();
        panelTitlu.setLayout(new FlowLayout());
        panelTitlu.add(new JLabel("Calculator Polinomial"));

        JPanel panelInput1 = new JPanel();
        panelInput1.setLayout(new FlowLayout());
        panelInput1.add(new JLabel("Polinom 1 = "));
        panelInput1.add(inPoli1);

        JPanel panelInput2 = new JPanel();
        panelInput2.setLayout(new FlowLayout());
        panelInput2.add(new JLabel("Polinom 2 = "));
        panelInput2.add(inPoli2);

        JPanel panelButoane = new JPanel();
        panelButoane.setLayout(new GridLayout(4, 1));
        panelButoane.add(adunareBtn);
        panelButoane.add(scadereBtn);
        panelButoane.add(inmultireBtn);
        panelButoane.add(impartireBtn);
        panelButoane.add(derivareBtn);
        panelButoane.add(integrareBtn);

        JPanel panelOutput = new JPanel();
        panelOutput.setLayout(new GridLayout(0,1));
        panelOutput.add(new JLabel("Rezultat"));
        panelOutput.add(outPoli);
        panelOutput.add(clearBtn);

        JPanel panelFinal = new JPanel();
        panelFinal.setLayout(new GridLayout(0, 1));
        panelFinal.add(panelTitlu);
        panelFinal.add(panelInput1);
        panelFinal.add(panelInput2);
        panelFinal.add(panelButoane);
        panelFinal.add(panelOutput);

        this.setContentPane(panelFinal);
        this.pack();
        this.setTitle("Calculator polinomial");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public String getUserInput(boolean polinom){
        if(polinom)
            return inPoli1.getText();
        return inPoli2.getText();
    }

    public void addPoliListener(ActionListener mal){
        adunareBtn.addActionListener(mal);
    }

    public void substrPoliListener(ActionListener mal){
        scadereBtn.addActionListener(mal);
    }

    public void clearListener(ActionListener mal){
        clearBtn.addActionListener(mal);
    }

    public void multiplyPoliListener(ActionListener mal){
        inmultireBtn.addActionListener(mal);
    }

    public void derivataPoliListener(ActionListener mal){
        derivareBtn.addActionListener(mal);
    }

    public void integralaPoliListener(ActionListener mal){
        integrareBtn.addActionListener(mal);
    }

    public void dividePoliListener(ActionListener mal){
        impartireBtn.addActionListener(mal);
    }

    public void reset(){
        inPoli1.setText("");
        inPoli2.setText("");
        outPoli.setText("");
    }

    public void setTotal(Polinom result){
        model.setTotal(result);
        if(result.toString().compareTo("")!=0)
            outPoli.setText(result.toString());
        else
            outPoli.setText(Float.toString(0.f));
    }

    public void setTotalD(Polinom[] result){
        model.setTotalD(result);
        if(result[1].toString().compareTo("")!=0)
            outPoli.setText("Q = " + result[0].toString() + " ; R = " + result[1]);
        else
            if(result[0].toString().compareTo("")!=0)
                outPoli.setText("Q = " + result[0].toString() + " ; R = 0.0");
            else
                outPoli.setText("ERROR");
    }
}
